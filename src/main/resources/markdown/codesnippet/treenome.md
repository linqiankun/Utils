# treenome

## algorithm

1. AVL树实现算法1（AVLTree1）
2. AVL树实现算法2（AVLTree2）
3. 红黑树实现算法1（RBTree1）
4. 红黑树实现算法2（RBTree2）
5. 二叉平衡树实现算法（BSTree）
6. 普通二叉树6（Tree）

## test pass

1. AVL树算法1测试（AVLTree1Test）
2. 红黑树算法2测试（RBTree2Test）

---

## untest

1. AVL树实现算法2（AVLTree2）
2. 红黑树实现算法1（RBTree1）
3. 二叉平衡树实现算法（BSTree）
4. 普通二叉树（Tree）
