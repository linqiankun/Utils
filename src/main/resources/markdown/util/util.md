# util

## util

1. Excel导出工具（ExcelExportUtil）
2. EXcel导入工具（ExcelImportUtil）
3. 文件读取工具（FileReadUtil）
4. 文件写入工具（FileWriteUtil）
5. Http(Https)连接工具（HttpConnectionUtil、HttpUrlConnectionUtil）
6. Xml解析工具（XmlParseUtil）
7. 反射工具（InvokeUtil）
8. 省份城市工具（ProvinceCityUtil）
9. 数组排序工具（ArraySortUtil）
10. 汉字转拼音工具（ChineseToEnglish2Util）
11. 人名币转大写工具（RmbUppercaseUtil）

## test pass

1. Excel导出（ExeclTest）
2. Execl导入（ExeclTest）
3. 文件读取（FileTest）
4. 文件写入（FileTest）
5. http连接（HttpConnectionTest）
6. 反射（InvokeTest）
7. xml解析（XmlTest）
8. 省份城市工具（ProvinceCityTest）

---

## unfinished

1. 数组排序

## test bug

1. 文件写入工具_从流中写入文件（`FileWriteUtil#writeFromInputStream`）