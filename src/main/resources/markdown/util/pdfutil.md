# pdfutil

## util

1. 图片转pdf工具，多图片转pdf工具（PicToPdfUtil）
2. pdf转图片工具（PdfToPicUtil）
3. 多个pdf合成工具（MutilPdfToOnePdfUtil）
4. Html转Pdf工具（HtmlToPdfUtil）
5. Word转Pdf工具（WordToPdfUtil）
6. PPT转Pdf工具（PPTToPdfUtil）
7. Excel转Pdf工具（ExcelToPdfUtil）

## test pass

1. genPdf，生成pdf（GenPdf）
2. 多图片合一pdf（MultiToOneTest）
3. pdf转图片（PdfToPic）
4. 图片转pdf（PicToPdf）

---

## unfinished

1. Html转Pdf工具
2. Word转Pdf工具
3. PPT转Pdf工具
4. Excel转Pdf工具