package com.lin.dto;

import lombok.Data;

/**
 * @author linqiankun
 */
@Data
public class SenseCallbackDto {

    private String batchId;

    private String platformNo;

    private String jobId;

    private String phone;

    private String loanUsername;

    private String templateCode;

    private String callStartTime;

    private String callAnswerTime;

    private String callEndTime;

    private String result;

    // (0：成功；1：忙音（用户正忙）；2：无法接通（来电提醒）4：呼入限制；5：呼叫转移；6：关机；7：停机；8：空号；9：正在通话中；10：网络忙；11：振铃未接听；12：短忙音；13：长忙音；-1
    // ：振铃未接；-2：核对（用户忙）
    private int    resultCode;

    private String audio;

    private String endFlow;

    private String ivrResult;
    
    private String dialogue;

}
