package com.lin.dto;

import lombok.Data;

/**
 * @author linqiankun
 */
@Data
public class BookDto {
    String name;
    String author;
    int price;
    String year;
    String language;
}
