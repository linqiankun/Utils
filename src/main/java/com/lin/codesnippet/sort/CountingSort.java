package com.lin.codesnippet.sort;

import java.util.Arrays;

/**
 * 计数排序
 *
 * @author linqiankun
 */
public class CountingSort {


    /**
     * 计数排序
     *
     * @param sourceArray 需要排序的数组
     * @return 返回排序后的数组
     */
    public static int[] countingsorted(int[] sourceArray) {
        // 对 arr 进行拷贝，不改变参数内容
        int[] arr = Arrays.copyOf(sourceArray, sourceArray.length);

        int maxValue = getMaxValue(arr);

        return sort(arr, maxValue);
    }

    /**
     * 统计排序
     *
     * @param arr      排序的数组
     * @param maxValue 最大值
     * @return 排序后的数组
     */
    private static int[] sort(int[] arr, int maxValue) {
        int bucketLen = maxValue + 1;
        int[] bucket = new int[bucketLen];

        for (int value : arr) {
            bucket[value]++;
        }

        int sortedIndex = 0;
        for (int j = 0; j < bucketLen; j++) {
            while (bucket[j] > 0) {
                arr[sortedIndex++] = j;
                bucket[j]--;
            }
        }
        return arr;
    }

    /**
     * 获取最大值
     *
     * @param arr 需要排序的数组
     * @return 最大值
     */
    private static int getMaxValue(int[] arr) {
        int maxValue = arr[0];
        for (int value : arr) {
            if (maxValue < value) {
                maxValue = value;
            }
        }
        return maxValue;
    }
}
