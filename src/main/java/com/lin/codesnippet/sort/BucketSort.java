package com.lin.codesnippet.sort;

import com.lin.enums.codesnippet.SortStatusCode;

import java.util.Arrays;

/**
 * 桶排序
 *
 * @author linqiankun
 */
public class BucketSort {


    /**
     * 桶排序
     *
     * @param sourceArray 需要排序的数组
     * @return 排序后的数组
     */
    public static int[] bucketSorted(int[] sourceArray) {
        // 对 arr 进行拷贝，不改变参数内容
        int[] arr = Arrays.copyOf(sourceArray, sourceArray.length);

        return bucketSort(arr, 5);
    }

    /**
     * 桶排序
     *
     * @param arr        需要排序的数组
     * @param bucketSize 桶大小
     * @return 排序后的数组
     */
    private static int[] bucketSort(int[] arr, int bucketSize) {
        if (arr.length == 0) {
            return arr;
        }

        int minValue = arr[0];
        int maxValue = arr[0];
        for (int value : arr) {
            if (value < minValue) {
                minValue = value;
            } else if (value > maxValue) {
                maxValue = value;
            }
        }

        int bucketCount = (int) Math.floor((maxValue - minValue) / bucketSize) + 1;
        int[][] buckets = new int[bucketCount][0];

        // 利用映射函数将数据分配到各个桶中
        for (int item : arr) {
            int index = (int) Math.floor((item - minValue) / bucketSize);
            buckets[index] = arrAppend(buckets[index], item);
        }

        int arrIndex = 0;
        for (int[] bucket : buckets) {
            if (bucket.length <= 0) {
                continue;
            }
            // 对每个桶进行排序，这里使用了插入排序
            InsertionSort.insertionSorted(bucket, SortStatusCode.MIX_SORT);
            // bucket = insertSort.sort(bucket);
            for (int value : bucket) {
                arr[arrIndex++] = value;
            }
        }

        return arr;
    }

    /**
     * 自动扩容，并保存数据
     *
     * @param arr   桶
     * @param value 值
     * @return 桶
     */
    private static int[] arrAppend(int[] arr, int value) {
        arr = Arrays.copyOf(arr, arr.length + 1);
        arr[arr.length - 1] = value;
        return arr;
    }

}
