package com.lin.codesnippet.sort;

import com.lin.enums.codesnippet.SortStatusCode;
import com.lin.codesnippet.sort.util.SortUtil;

/**
 * 选择排序
 * <p>O(n^2)~O(n^2)~O(n^2)</p>
 * 不稳定
 *
 * @author linqiankun
 */
public class SelectionSort {


    /**
     * 选择排序
     *
     * @param array          需要排序的数组
     * @param sortStatusCode 排序方式
     */
    public static void selectionSorted(int[] array, SortStatusCode sortStatusCode) {
        //确定最小的位置
        for (int i = 0; i < array.length; i++) {
            int min = i;
            //确定最小的位置存放的数据
            for (int j = i + 1; j < array.length; j++) {
                //用标志为查出为排序队列中最小/大的
                if (sortStatusCode.getCode() == SortUtil.compare(array[j], array[min])) {
                    min = j;
                }
            }
            //将最小的放在最前面
            if (min != i) {
                SortUtil.swap(array, i, min);
            }

        }


    }


}
