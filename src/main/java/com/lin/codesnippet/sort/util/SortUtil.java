package com.lin.codesnippet.sort.util;

/**
 * 排序工具
 *
 * @author linqiankun
 */
public class SortUtil {

    /**
     * 交换
     *
     * @param array 数组
     * @param a     索引
     * @param b     索引
     */
    public static void swap(int[] array, int a, int b) {
        int temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }

    /**
     * 比较器
     *
     * @param a 索引
     * @param b 索引
     * @return 结果，0，—1，1
     */
    public static int compare(int a, int b) {
        return Integer.compare(a, b);
    }

}
