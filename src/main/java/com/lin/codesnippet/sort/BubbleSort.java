package com.lin.codesnippet.sort;

import com.lin.enums.codesnippet.SortStatusCode;
import com.lin.codesnippet.sort.util.SortUtil;

/**
 * 冒泡排序
 * <p>O(n)~O(n^2)~O(n^2)</p>
 * 稳定
 *
 * @author linqiankun
 */
public class BubbleSort {


    /**
     * 冒泡排序
     *
     * @param array          需要排序的数组
     * @param sortStatusCode 排序方式
     */
    public static void bubbleSorted(int[] array, SortStatusCode sortStatusCode) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                int falg = SortUtil.compare(array[i], array[j]);
                //可修改排序方向
                if (falg == sortStatusCode.getCode()) {
                    SortUtil.swap(array, i, j);
                }
            }
        }
    }


    /**
     * 鸡尾酒排序
     *
     * @param array 需要排序的数组
     * @param n     数组大小
     */
    public static void CocktailSort(int[] array, int n) {
        // 初始化边界
        int left = 0;
        int right = n - 1;
        while (left < right) {
            // 前半轮,将最大元素放到后面
            for (int i = left; i < right; i++) {
                if (array[i] > array[i + 1]) {
                    SortUtil.swap(array, i, i + 1);
                }
            }
            right--;
            // 后半轮,将最小元素放到前面
            for (int i = right; i > left; i--) {
                if (array[i - 1] > array[i]) {
                    SortUtil.swap(array, i - 1, i);
                }
            }
            left++;
        }
    }


}
