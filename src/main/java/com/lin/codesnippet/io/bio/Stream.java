package com.lin.codesnippet.io.bio;

import java.io.*;

/**
 * @author 九分石人
 */
public class Stream {

    /**
     * 通过IO流进行文件传输
     *
     * @throws IOException 文件传输异常
     */
    private static void fileCopyStream() throws IOException {
        File file = new File("D:\\util\\BingWallpaper.jpg");
        File filecopy = new File("D:\\util\\BingWallpaper-copy-bio.jpg");
        FileInputStream fileInputStream = new FileInputStream(file);
        FileOutputStream fileOutputStream = new FileOutputStream(filecopy);
        byte[] bytes = new byte[1024];
        while (fileInputStream.read(bytes) > 0) {
            fileOutputStream.write(bytes);
            fileOutputStream.flush();
        }
        fileInputStream.close();
        fileOutputStream.close();
    }
}
