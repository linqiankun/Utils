package com.lin.codesnippet.io.nio;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author 九分石人
 */
public class Channel {


    /**
     * 通过NIO通道进行文件拷贝
     *
     * @throws IOException 文件异常
     */
    private static void fileCopyChannel() throws IOException {
        File file = new File("D:\\util\\BingWallpaper.jpg");
        File filecopy = new File("D:\\util\\BingWallpaper-copy.jpg");
        FileChannel fileChannel = new FileInputStream(file).getChannel();
        FileChannel fileChannel1 = new FileOutputStream(filecopy).getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        while (fileChannel.read(byteBuffer) > 0) {
            byteBuffer.flip();
            fileChannel1.write(byteBuffer);
            byteBuffer.clear();
        }
        fileChannel.close();
        fileChannel1.close();
    }


}
