package com.lin.codesnippet.io.aio;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.Future;

/**
 * @author 九分石人
 */
public class Asynchronous {


    public static void main(String[] args) throws IOException {
        File file = new File("D:\\util\\BingWallpaper.jpg");
        OpenOption options = StandardOpenOption.READ;
        AsynchronousFileChannel asynchronousFileChannel = AsynchronousFileChannel.open(file.toPath(), options);
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        Future<Integer> read = asynchronousFileChannel.read(buffer, 1);

        try (InputStream inputStream = new FileInputStream(file)) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringReader stringReader = new StringReader("1");
        }


    }


}
