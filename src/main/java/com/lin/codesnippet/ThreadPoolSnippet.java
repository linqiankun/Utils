package com.lin.codesnippet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 四种线程池模板
 * @author 九分石人
 */
public class ThreadPoolSnippet {

    private static final Integer threadNum = 5;

    /**
     * 缓存线程池
     */
    public void newCachedThreadPool() {
        ExecutorService executorService = Executors.newCachedThreadPool();

        for (int i = 0; i < threadNum; i++) {
            final int index = i;
            executorService.execute(() -> {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat(
                            "HH:mm:ss");
                    System.out.println("运行时间: " +
                            sdf.format(new Date()) + " " + index + " " + Thread.currentThread().getName());
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        executorService.shutdown();
    }

    /**
     * 单线程线程池
     */
    public void newSingleThreadExecutor() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        execute(executorService);
        executorService.shutdown();
    }

    /**
     * 定长线程池
     */
    public void newFixedThreadPool() {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        execute(executorService);
    }

    private void execute(ExecutorService executorService) {
        for (int i = 0; i < threadNum; i++) {
            final int index = i;
            executorService.execute(() -> {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat(
                            "HH:mm:ss");
                    System.out.println("运行时间: " +
                            sdf.format(new Date()) + " " + index + " " + Thread.currentThread().getName());
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    /**
     * 定时执行的线程池
     */
    public void newScheduledThreadPool() {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(3);
        SimpleDateFormat sdf = new SimpleDateFormat(
                "HH:mm:ss");
        for (int i = 0; i < threadNum; i++) {
            final int index = i;
            System.out.println("提交时间: " + sdf.format(new Date()));
            executorService.schedule(() -> {
                try {

                    System.out.println("运行时间: " +
                            sdf.format(new Date()) + " " + index + " " + Thread.currentThread().getName());
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, 3, TimeUnit.SECONDS);

        }
    }

    /**
     * 周期执行的线程池
     *
     * @throws InterruptedException
     */
    public void newScheduledThreadPool2() throws InterruptedException {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(3);
        SimpleDateFormat sdf = new SimpleDateFormat(
                "HH:mm:ss");
//        for (int i = 0; i < threadNum; i++) {
        final int index = 1;
        System.out.println("提交时间: " + sdf.format(new Date()));
        executorService.scheduleAtFixedRate(() -> {
            try {

                System.out.println("运行时间: " +
                        sdf.format(new Date()) + " " + index + " " + Thread.currentThread().getName());
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, 1, 3, TimeUnit.SECONDS);
        //主线程等待10秒钟后关闭
        Thread.sleep(10000);
//        }

        executorService.shutdown();
    }


}
