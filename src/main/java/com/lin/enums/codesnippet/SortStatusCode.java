package com.lin.enums.codesnippet;

/**
 * 排序状态吗
 *
 * @author linqiankun
 */
public enum SortStatusCode {


    /**
     * 小到大排序
     */
    MIX_SORT(-1, "小到大"),
    /**
     * 大到小排序
     */
    MAX_SORT(1, "大到小"),
    /**
     * 两值相等
     */
    EQUAL(0, "相等");


    int code;
    String desc;

    SortStatusCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
