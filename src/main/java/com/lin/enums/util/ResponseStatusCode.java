package com.lin.enums.util;

/**
 * http相应响应状态码
 *
 * @author linqiankun
 */
public enum ResponseStatusCode {


    /**
     * 200，请求成功成功
     */
    OK(200, "ok"),

    /**
     * 404，资源未找到
     */
    NOT_EXIST(404, "notfound");

    int code;
    String desc;

    ResponseStatusCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
