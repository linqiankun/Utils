package com.lin.util.pdfutil;

import com.itextpdf.text.*;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * @author linqiankun
 */
@Slf4j
public class MultiPdfToOnePdfUtil {


    /**
     * 将多个图片转成一个pdf
     *
     * @param imageFolderPath 图片文件夹地址
     * @param pdfPath         pdf路径
     */
    public static void picToPdf(String imageFolderPath, String pdfPath) {
        try {
            // 图片地址
            String imagePath;
            // 获取图片文件夹对象
            File filePath = new File(imageFolderPath);
            File[] files = filePath.listFiles();
            // 循环获取图片文件夹内的图片
            assert files != null;

            // 输入流
            FileOutputStream fos = new FileOutputStream(pdfPath);
            // 创建文档
            Document doc = new Document();
            doc.setMargins(0, 0, 0, 0);
            // 写入PDF文档
            PdfWriter.getInstance(doc, fos);
            // 读取图片流
            BufferedImage img;
            // 实例化图片
            Image image;
            doc.open();

            for (File file : files) {
                if (file.getName().toLowerCase().endsWith(".png")
                        || file.getName().toLowerCase().endsWith(".jpg")
                        || file.getName().toLowerCase().endsWith(".gif")
                        || file.getName().toLowerCase().endsWith(".jpeg")
                        || file.getName().toLowerCase().endsWith(".tif")) {
                    imagePath = imageFolderPath + File.separator + file.getName();
                    System.out.println(file.getName());
                    // 读取图片流
                    img = ImageIO.read(new File(imagePath));
                    // 根据图片大小设置文档大小
                    // doc.setPageSize(new Rectangle(img.getWidth(), img.getHeight()));
                    doc.setPageSize(PageSize.A4);
                    doc.newPage();
                    // 实例化图片
                    image = Image.getInstance(imagePath);
                    image.scaleToFit(PageSize.A4);
                    // 添加图片到文档
                    doc.add(image);
                }
            }
            // 关闭文档
            doc.close();
        } catch (IOException | DocumentException e) {
            log.error(e.getMessage());
        }
    }


    /**
     * 多个pdf合一工具，pdf大小不一
     *
     * @param bytes 需要合成的pdf数组
     * @return 合成后的pdf
     * @throws DocumentException 文本异常
     * @throws IOException       流异常
     */
    public static byte[] pdfmMix(List<byte[]> bytes) throws DocumentException, IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        // 创建一个新的PDF
        Document document = new Document();
        PdfCopy copy = new PdfCopy(document, bos);
        document.open();
        // 取出单个PDF的数据
        int count = 0;
        for (byte[] bs : bytes) {
            PdfReader reader = new PdfReader(bs);
            int pageTotal = reader.getNumberOfPages();
            log.info("pdf的页码数是 ==> {}", pageTotal);
            for (int pageNo = 1; pageNo <= pageTotal; pageNo++) {
                document.newPage();
                PdfImportedPage page = copy.getImportedPage(reader, pageNo);
                copy.addPage(page);
            }
            reader.close();
            log.info("已完成的pdf ==> {}",++count);
        }
        document.close();
        byte[] pdfs = bos.toByteArray();
        bos.close();
        copy.close();
        return pdfs;
    }


}
