package com.lin.util;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * 反射的工具类
 *
 * @author 九分石人, 2020-02-21
 */
@Slf4j
public class InvokeUtil {

    /**
     * 将Map中的数据反射到对象中
     *
     * @param <T> 要反射的对象类型
     * @param t   反射的对象
     * @param map 属性值的集合
     * @author com.lin, 2020-02-21
     */
    public static <T> void setFromMap(T t, Map<String, String> map) {
        Class c = t.getClass();
        Field[] fields = c.getDeclaredFields();
        try {
            for (Field f : fields) {
                setFormatValue(t, map, f);
            }
        } catch (IllegalAccessException e) {
            log.error(e.getMessage());
        }

    }

    /**
     * 将不同格式的数据按字段反射进对象中
     *
     * @param t   反射的类对象
     * @param map 对象值的集合
     * @param f   字段的名称
     * @param <T> 反射对象的类型
     * @throws IllegalAccessException 参数非法异常
     * @author com.lin, 2020-02-22
     */
    private static <T> void setFormatValue(T t, Map<String, String> map, Field f) throws IllegalAccessException {
        f.setAccessible(true);
        String key = f.toString().substring(f.toString().lastIndexOf(".") + 1);
        String typeName = f.getGenericType().getTypeName();
        switch (typeName) {
            case "boolean":
                f.setBoolean(t, Boolean.parseBoolean(map.get(key)));
                break;
            case "java.lang.Boolean":
                f.set(t, Boolean.parseBoolean(map.get(key)));
                break;
            case "byte":
                f.setByte(t, Byte.parseByte(map.get(key)));
                break;
            case "java.lang.Byte":
                f.set(t, Byte.parseByte(map.get(key)));
                break;
            case "char":
                f.setChar(t, map.get(key).charAt(0));
                break;
            case "java.lang.Character":
                f.set(t, map.get(key).charAt(0));
                break;
            case "double":
                f.setDouble(t, Double.parseDouble(map.get(key)));
                break;
            case "java.lang.Double":
                f.set(t, Double.parseDouble(map.get(key)));
                break;
            case "float":
                f.setFloat(t, Float.parseFloat(map.get(key)));
                break;
            case "java.lang.Float":
                f.set(t, Float.parseFloat(map.get(key)));
                break;
            case "int":
                f.setInt(t, Integer.parseInt(map.get(key)));
                break;
            case "java.lang.Integer":
                f.set(t, Integer.parseInt(map.get(key)));
                break;
            case "long":
                f.setLong(t, Long.parseLong(map.get(key)));
                break;
            case "java.lang.Long":
                f.set(t, Long.parseLong(map.get(key)));
                break;
            case "short":
                f.setShort(t, Short.parseShort(map.get(key)));
                break;
            case "java.lang.Short":
                f.set(t, Short.parseShort(map.get(key)));
                break;
            case "java.lang.String":
                f.set(t, map.get(key));
                break;
            default:
                break;
        }
    }

}
