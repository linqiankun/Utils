package com.lin.util;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.io.*;

/**
 * 文件读取工具
 *
 * @author 九分石人，2019-12-31
 */
@Slf4j
public class FileReadUtil {

    /**
     * 通过文件获取File对象
     *
     * @param filePath 文件路径
     * @return 文件内容
     * @author com.lin, 2019-02-18
     */
    public static File readToFile(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            return file;
        }
        return null;
    }


    /**
     * 将文本文件中的内容读入到buffer中
     *
     * @param buffer   buffer
     * @param filePath 文件路径
     * @author com.lin, 2019-12-18
     */
    private static void readToBuffer(StringBuffer buffer, String filePath) {
        InputStream is = null;
        BufferedReader reader = null;
        try {
            is = new FileInputStream(filePath);
            reader = new BufferedReader(new InputStreamReader(is));
            // 用来保存每行读取的内容
            String line;
            // 读取第一行
            line = reader.readLine();

            // 如果 line 为空说明读完了
            while (line != null) {
                // 将读到的内容添加到 buffer 中
                buffer.append(line);
                // 添加换行符
                buffer.append("\n");
                // 读取下一行
                line = reader.readLine();
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }

    /**
     * 读取文本文件内容到字符串
     *
     * @param filePath 文件所在路径
     * @return 文本内容
     * @author com.lin, 2019-12-19
     */
    @NotNull
    public static String readFileToString(String filePath) {
        StringBuffer sb = new StringBuffer();
        FileReadUtil.readToBuffer(sb, filePath);
        return sb.toString();
    }

}
