package com.lin.localhost;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Slf4j
public class FileRename {


    public static void fileRename(File file) throws IOException {
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File f : files) {
                    fileRename(f);
                }
            }
            if (file.isFile()) {
                String name = file.getName();
                if (name.endsWith(".mp4")) {
                    String parent = file.getParent();
                    System.out.println(parent);
                    String[] s = name.split("_");
                    File titlefile = new File(parent + "/" + s[0] + ".info");
                    try {
                        FileReader reader = new FileReader(titlefile);
                        BufferedReader bufferedReader = new BufferedReader(reader);
                        String s1 = bufferedReader.readLine();
                        JSONObject jsonObject = JSONObject.parseObject(s1);
                        String title = (String) jsonObject.get("Title");
                        file.renameTo(new File(parent + File.separator + title.replaceAll("/", "-").replaceAll(" ", "") + ".mp4"));
//                        File newfile = new File(parent + File.separator + title.trim().replaceAll("/","-") + ".mp4");
//                        newfile.createNewFile();
//                        try (FileInputStream fileInputStream = new FileInputStream(file)) {
//                            try (FileOutputStream fileOutputStream = new FileOutputStream(newfile)) {
//                                byte[] bytes = new byte[1024];
//                                while (fileInputStream.read(bytes) > 0) {
//                                    fileOutputStream.write(bytes);
//                                }
//                            }
//                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }
        }
    }

    public static void fileNameTrim(File file) throws IOException {
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File f : files) {
                    fileNameTrim(f);
                }
            }
            if (file.isFile()) {
                String name = file.getName();
                if (name.endsWith(".mp4")) {
                    String fileName = file.getName();
                    String parent = file.getParent();
                    File newfile = new File(parent + File.separator + fileName.replaceAll("/", "-").replaceAll(" ", ""));
//                    while (newfile.exists()) {
//                        String fileName2 = parent + File.separator + newfile.getName();
//                        String newfileName = fileName2.substring(0, fileName2.lastIndexOf(".")) + "(1).mp4";
//                        newfile = new File(newfileName);
//                    }
                    file.renameTo(newfile);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        fileRename(new File("D:\\Videos\\BiliBliVideo\\舞蹈"));
//        fileNameTrim(new File(("D:\\Videos\\BiliBliVideo\\万兴素材")));
    }

}
