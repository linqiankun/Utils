package com.lin.testcodesnippet.TestIO.qq.qq1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author 九分石人
 */
public class NIOServer {


    static Selector serverSelector;
    static Selector clientSelector;

    static {
        try {
            serverSelector = Selector.open();
            clientSelector = Selector.open();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {


        NIOServer nioServer = new NIOServer();
        new Thread(() -> {
            try {
                ServerSocketChannel socketChannel = ServerSocketChannel.open();
                socketChannel.socket().bind(new InetSocketAddress(3333));
                socketChannel.configureBlocking(false);
                socketChannel.register(serverSelector, SelectionKey.OP_ACCEPT);
                nioServer.acceptListener();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                nioServer.clientListener();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

    }

    public void acceptListener() throws IOException {
        while (true) {
            if (serverSelector.select(1) > 0) {
                Set<SelectionKey> selectionKeys = serverSelector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey next = iterator.next();
                    if (next.isAcceptable()) {
                        try {
                            SocketChannel accept = ((ServerSocketChannel) next.channel()).accept();
                            accept.configureBlocking(false);
                            accept.register(clientSelector, SelectionKey.OP_READ);
                        } finally {
                            iterator.remove();
                        }
                    }
                }
            }
        }
    }

    /**
     * 客户端消息的监听
     *
     * @throws IOException
     */
    public void clientListener() throws IOException {
        while (true) {
            if (clientSelector.select(1) > 0) {
                Set<SelectionKey> selectionKeys = clientSelector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey next = iterator.next();
                    if (next.isReadable()) {
                        try {
                            SocketChannel channel = (SocketChannel) next.channel();
                            ByteBuffer buffer = ByteBuffer.allocate(1024);
                            channel.read(buffer);
                            buffer.flip();
                            System.out.println(LocalDateTime.now().toString() + " Server 端接收到来自 Client 端的消息: " +
                                    Charset.defaultCharset().decode(buffer).toString());
                        } finally {
                            iterator.remove();
                            next.interestOps(SelectionKey.OP_READ);
                        }
                    }
                }
            }
        }


    }


}
