package com.lin.testcodesnippet.TestIO.qq.qq1;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

/**
 * @author 九分石人
 */
public class NIOClient {


    public static void main(String[] args) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress(InetAddress.getLocalHost(), 3333));
        socketChannel.configureBlocking(false);
        while (true) {
            Scanner sc = new Scanner(System.in);
            String msg = sc.next();
            if (msg.equals("return")) {
                break;
            }
            buffer.put(msg.getBytes());
            buffer.flip();
            socketChannel.write(buffer);
            buffer.clear();
        }
    }
}
