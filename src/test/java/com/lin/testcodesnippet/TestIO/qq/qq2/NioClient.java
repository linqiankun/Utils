package com.lin.testcodesnippet.TestIO.qq.qq2;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * nio客户端
 *
 * @author linqiankun
 */
public class NioClient {

    public static void main(String[] args) throws Exception {

        // 得到一个网络通道
        SocketChannel socketChannel = SocketChannel.open();
        // 设置非阻塞模式
        socketChannel.configureBlocking(false);
        // 提供服务器端的ip地址和端口号
        InetSocketAddress inetSocketAddress = new InetSocketAddress("127.0.0.1", 9001);
        // 连接服务器
        if (!socketChannel.connect(inetSocketAddress)) {
            // 循环-直到某一时刻连接到(在连接的时候可以做别的事情，比如下面打印数据，这就是NIO的优势)
            while (!socketChannel.finishConnect()) {
                System.out.println("还没有连接到。。。");
            }
            System.out.println("连接到了。。。");
        }
        // 得到一个缓冲区并存入数据
        String string = "NIO测试，我是客户端传的数据";
        ByteBuffer writeBuffer = ByteBuffer.wrap(string.getBytes());
        // 发送数据
        socketChannel.write(writeBuffer);
        // 这里关闭通道，服务器端会报异常，所以这里就先设置阻塞
        System.in.read();
    }
}
