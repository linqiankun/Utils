package com.lin.testcodesnippet.TestIO.qq.qq2;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/**
 * nio服务端
 *
 * @author linqiankun
 */
public class NioServer {

    public static void main(String[] args) throws Exception {

        // 得到一个ServerSocketChannel对象
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        // 得到一个Selector对象
        Selector selector = Selector.open();
        // 绑定一个端口号
        serverSocketChannel.bind(new InetSocketAddress(9001));
        // 设置非阻塞式
        serverSocketChannel.configureBlocking(false);
        // 把ServerSocketChannel对象注册给Selector对象
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        // 做事
        for (; ; ) {
            // 监控客户端
            if (selector.select(2000) == 0) {
                System.out.println("没有客户端连接我，我可以干别的事情");
                continue;
            }

            // 得到SelectionKey，判断通道里的事件
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();

                // 如果是客户端连接事件
                if (key.isAcceptable()) {
                    System.out.println("连接事件");
                    // 接收连接
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    // 设置非阻塞式
                    socketChannel.configureBlocking(false);
                    // 注册
                    socketChannel.register(selector, SelectionKey.OP_READ, ByteBuffer.allocate(1024));
                }

                // 读取客户端数据事件
                if (key.isReadable()) {
                    SocketChannel channel = (SocketChannel) key.channel();
                    ByteBuffer buffer = (ByteBuffer) key.attachment();
                    channel.read(buffer);
                    System.out.println("客户端发来数据：" + new String(buffer.array()));
                }

                // 手动从集合中移除当前key，防止重复处理
                iterator.remove();
            }
        }
    }
}

