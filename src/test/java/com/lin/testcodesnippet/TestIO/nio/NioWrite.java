package com.lin.testcodesnippet.TestIO.nio;

import org.junit.Test;

import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * nio写入文件
 *
 * @author linqiankun
 */
public class NioWrite {

    @Test
    public void testNioWrite() throws Exception {

        // 创建输出流
        FileOutputStream fileOutputStream = new FileOutputStream("test.txe");
        // 从流中得到一个通道
        FileChannel channel = fileOutputStream.getChannel();
        // 提供一个缓冲区
        ByteBuffer allocate = ByteBuffer.allocate(1024);
        // 往缓冲区存入数据
        String string = "我是要输入的数据";
        allocate.put(string.getBytes());
        // 翻转缓冲区，将位置设为初始位置
        allocate.flip();
        // 把缓冲区写到通道中
        channel.write(allocate);
        // 关闭
        fileOutputStream.close();
    }
}
