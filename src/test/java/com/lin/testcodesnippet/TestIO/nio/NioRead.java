package com.lin.testcodesnippet.TestIO.nio;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * nio读取文件
 *
 * @author linqiankun
 */
public class NioRead {

    @Test
    public void testNioRead() throws Exception {

        File file = new File("test.txe");
        // 创建输入流
        FileInputStream fileInputStream = new FileInputStream(file);
        // 得到一个通道
        FileChannel channel = fileInputStream.getChannel();
        // 准备一个缓冲区
        ByteBuffer allocate = ByteBuffer.allocate((int) file.length());
        // 从通道里读取数据并存到缓冲区
        channel.read(allocate);
        // 打印数据
        System.out.println(new String(allocate.array()));
        // 关闭
        fileInputStream.close();
    }
}
