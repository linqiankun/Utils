package com.lin.testcodesnippet.TestSort;

import com.lin.enums.codesnippet.SortStatusCode;
import com.lin.codesnippet.sort.BubbleSort;
import org.junit.Test;

/**
 * 冒泡排序
 *
 * @author linqiankun
 */
public class BubbleSortTest {


    @Test
    public void bubbleSortedTest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        BubbleSort.bubbleSorted(a, SortStatusCode.MAX_SORT);
        for (int i : a) {
            System.out.println(i);
        }

    }

}
