package com.lin.testcodesnippet.TestSort;

import com.lin.enums.codesnippet.SortStatusCode;
import com.lin.codesnippet.sort.ShellSort;
import org.junit.Test;

/**
 * 希尔排序
 *
 * @author linqiankun
 */
public class ShellSortTest {

    @Test
    public void shellSortTest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        ShellSort.shellSorted(a, SortStatusCode.MIX_SORT);
        for (int i : a) {
            System.out.println(i);
        }
    }


}
