package com.lin.testcodesnippet.TestSort;

import com.lin.enums.codesnippet.SortStatusCode;
import com.lin.codesnippet.sort.InsertionSort;
import org.junit.Test;

/**
 * 插入排序
 *
 * @author linqiankun
 */
public class InsertionSortTest {


    @Test
    public void insertionSortedtest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        InsertionSort.insertionSorted(a, SortStatusCode.MIX_SORT);
        for (int i : a) {
            System.out.println(i);
        }
    }

    @Test
    public void insertionSortedDichotomyTest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        InsertionSort.insertionSortedDichotomy(a, SortStatusCode.MIX_SORT);
        for (int i : a) {
            System.out.println(i);
        }
    }


}
