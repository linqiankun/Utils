package com.lin.testcodesnippet.TestSort;

import com.lin.enums.codesnippet.SortStatusCode;
import com.lin.codesnippet.sort.HeapSort;
import org.junit.Test;

/**
 * 堆排序
 *
 * @author linqiankun
 */
public class HeapSortTest {


    @Test
    public void heapSrotTest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        HeapSort.heapSorted(a, SortStatusCode.MAX_SORT);
        for (int i : a) {
            System.out.println(i);
        }
    }

}
