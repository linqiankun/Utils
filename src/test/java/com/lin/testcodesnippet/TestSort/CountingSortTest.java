package com.lin.testcodesnippet.TestSort;

import com.lin.codesnippet.sort.CountingSort;
import org.junit.Test;

/**
 * 计数排序
 *
 * @author linqiankun
 */
public class CountingSortTest {


    @Test
    public void countingSortedTest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        int[] countingsorted = CountingSort.countingsorted(a);
        for (int i : countingsorted) {
            System.out.println(i);
        }
    }
}
