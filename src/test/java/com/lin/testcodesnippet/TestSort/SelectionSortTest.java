package com.lin.testcodesnippet.TestSort;

import com.lin.enums.codesnippet.SortStatusCode;
import com.lin.codesnippet.sort.SelectionSort;
import org.junit.Test;

/**
 * 选择排序
 *
 * @author linqiankun
 */
public class SelectionSortTest {


    @Test
    public void selectionSortedTest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        SelectionSort.selectionSorted(a, SortStatusCode.MIX_SORT);
        for (int i : a) {
            System.out.println(i);
        }
    }
}
