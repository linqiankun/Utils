package com.lin.testcodesnippet.TestSort;

import com.lin.codesnippet.sort.RadixSort;
import org.junit.Test;

/**
 * 基数排序
 *
 * @author linqiankun
 */
public class RadixSortTest {


    @Test
    public void radixSortedTest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        int[] countingsorted = RadixSort.radixSorted(a);
        for (int i : countingsorted) {
            System.out.println(i);
        }
    }


}
