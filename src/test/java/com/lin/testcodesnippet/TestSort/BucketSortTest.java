package com.lin.testcodesnippet.TestSort;

import com.lin.codesnippet.sort.BucketSort;
import org.junit.Test;

/**
 * 桶排序
 *
 * @author linqiankun
 */
public class BucketSortTest {


    @Test
    public void bucketSortedTest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        int[] countingsorted = BucketSort.bucketSorted(a);
        for (int i : countingsorted) {
            System.out.println(i);
        }
    }
}
