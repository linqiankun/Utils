package com.lin.testcodesnippet.TestSort;

import com.lin.enums.codesnippet.SortStatusCode;
import com.lin.codesnippet.sort.MergeSort;
import org.junit.Test;

/**
 * 归并排序
 *
 * @author linqiankun
 */
public class MergeSortTest {


    @Test
    public void mergeSortedRecursionTest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        MergeSort.mergeSortedRecursion(a, SortStatusCode.MAX_SORT, 0, a.length - 1);
        for (int i : a) {
            System.out.println(i);
        }
    }

    @Test
    public void mergeSortedIterationTest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        MergeSort.mergeSortedIteration(a, SortStatusCode.MIX_SORT);
        for (int i : a) {
            System.out.println(i);
        }
    }

}
