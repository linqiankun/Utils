package com.lin.testcodesnippet.TestSort;

import com.lin.enums.codesnippet.SortStatusCode;
import com.lin.codesnippet.sort.QuickSort;
import org.junit.Test;

/**
 * 快速排序
 *
 * @author linqiankun
 */
public class QuickSortedTest {


    @Test
    public void quickSortedTest() {
        int[] a = {1, 6, 8, 2, 4, 5, 7, 1, 0};
        QuickSort.quickSorted(a, 0, a.length - 1, SortStatusCode.MAX_SORT);
        for (int i : a) {
            System.out.println(i);
        }
    }

}
