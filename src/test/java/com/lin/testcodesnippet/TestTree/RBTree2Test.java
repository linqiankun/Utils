package com.lin.testcodesnippet.TestTree;

import com.lin.codesnippet.treeNome.rbtree.RBTree2;
import org.junit.Test;

/**
 * 红黑树测试
 *
 * @author linqiankun
 */
public class RBTree2Test {

    /**
     * test pass
     */
    @Test
    public void testRBTree2() {
        RBTree2 T = new RBTree2();
        RBTree2.RedBlackTreeNode node1 = T.RB_NODE(10);
        T.RB_INSERT(T, node1);
        RBTree2.RedBlackTreeNode node2 = T.RB_NODE(20);
        T.RB_INSERT(T, node2);
        RBTree2.RedBlackTreeNode node3 = T.RB_NODE(30);
        T.RB_INSERT(T, node3);
        T.preorder(T.getRoot());
    }


}
