package com.lin.testcodesnippet.TestTree;

import com.lin.codesnippet.treeNome.avltree.AVLTree1;
import org.junit.Test;

/**
 * AVL树测试
 *
 * @author linqiankun
 */
public class AVLTree1Test {

    /**
     * test pass
     */
    @Test
    public void testAVLTree1() {
        AVLTree1<Integer> avl = new AVLTree1<>();
        /*可自行添加插入，删除操作进行测试*/
        avl.insert(3);
        avl.insert(5);
        avl.insert(6);
        avl.insert(7);
        avl.insert(8);
        avl.insert(9);
        avl.preorderTraverse();
        System.out.println();
        System.out.println(avl.size());

        avl.delete(7);
        avl.delete(8);
        avl.preorderTraverse();
        System.out.println();
        System.out.println(avl.size());
    }


}
