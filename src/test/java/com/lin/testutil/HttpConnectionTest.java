package com.lin.testutil;

import com.lin.util.HttpConnectionUtil;
import com.lin.util.HttpUrlConnectionUtil;
import org.junit.Test;

/**
 * http连接测试
 *
 * @author linqiankun
 */
public class HttpConnectionTest {

    /**
     * test pass
     */
    @Test
    public void httpConnectionTest() {
        String get = HttpConnectionUtil.sendGet("http://www.baidu.com");
        System.out.println(get);
        String post = HttpConnectionUtil.sendPost("http://www.baidu.com", "");
        System.out.println(post);
    }

    /**
     * test pass
     */
    @Test
    public void httpUrlConnectionTest() {
        String get = HttpUrlConnectionUtil.doGet("http://www.baidu.com");
        System.out.println(get);
        String post = HttpUrlConnectionUtil.doPost("http://www.baidu.com", "");
        System.out.println(post);
    }


}
