package com.lin.testutil;


import com.lin.util.ProvinceCityUtil;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * 根据地址解析城市测试
 *
 * @author linqiankun
 */
public class ProvinceCityTest {

    /**
     * test pass
     */
    @Test
    public void provinceCityTest() {
        List<Map<String, String>> maps = ProvinceCityUtil.addressResolution("北京市市辖区丰台区");
        System.out.println(maps);
    }

    /**
     * test pass
     */
    @Test
    public void provinceCityTestNo() {
        Map<String, String> maps = ProvinceCityUtil.addressResolutionNoTwo("内蒙古维吾尔组自治区胡琦市");
        System.out.println(maps);
    }
}
