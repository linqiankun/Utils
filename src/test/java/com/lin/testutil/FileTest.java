package com.lin.testutil;

import com.lin.util.FileReadUtil;
import com.lin.util.FileWriteUtil;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * 文件读取测试，将读取到的文件反射进对象集合中
 *
 * @author linqiankun
 */
public class FileTest {

    private static final String filePath = "temp/fileString.txt";

    /**
     * test pass
     *
     * @throws IOException
     */
    @Test
    public void readFileToStringTest() throws IOException {
        String s = FileReadUtil.readFileToString(filePath);
        System.out.println(s);
    }

    /**
     * test pass
     */
    @Test
    public void writeFromStringTest() {
        String s = "wertyukmcdrtyukmnhgfrtyu";
        FileWriteUtil.writeFromString(s, filePath);
    }

    /**
     * test bug
     */
    @Test
    public void writeFromInputStreamTest() {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("book.xml");
        FileWriteUtil.writeFromInputStream(in, filePath);
    }

}