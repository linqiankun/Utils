package com.lin.testutil.testpdfutil;

import com.lin.util.FileWriteUtil;
import com.lin.util.pdfutil.PdfToPicUtil;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * pdf转图片
 *
 * @author linqiankun
 */
public class PdfToPicTest {

    private static final String filePath = "temp/";

    /**
     * test pass
     *
     * @throws IOException
     */
    @Test
    public void testOne() throws IOException {
        FileInputStream in = new FileInputStream(filePath + "mutil1.pdf");
        List<ByteArrayOutputStream> byteArrayOutputStreams = PdfToPicUtil.pdfToPic1(in);
        for (int i = 0; i < byteArrayOutputStreams.size(); i++) {
            String newfilePath = filePath + i + 1 + ".jpg";
            FileWriteUtil.canCreatFile(newfilePath);
            FileOutputStream outputStream = new FileOutputStream(newfilePath);
            ByteArrayOutputStream byteArrayOutputStream = byteArrayOutputStreams.get(i);
            byte[] bytes = byteArrayOutputStream.toByteArray();
            outputStream.write(bytes);
            outputStream.flush();
            byteArrayOutputStream.close();
            outputStream.close();
        }
    }

    /**
     * test pass
     *
     * @throws IOException
     */
    @Test
    public void testTwo() throws IOException {
        FileInputStream in = new FileInputStream(filePath + "mutil1.pdf");
        List<ByteArrayOutputStream> byteArrayOutputStreams = PdfToPicUtil.pdfToPic2(in);
        for (int i = 0; i < byteArrayOutputStreams.size(); i++) {
            String newfilePath = filePath + i + 2 + ".PNG";
            FileWriteUtil.canCreatFile(newfilePath);
            FileOutputStream outputStream = new FileOutputStream(newfilePath);
            ByteArrayOutputStream byteArrayOutputStream = byteArrayOutputStreams.get(i);
            byte[] bytes = byteArrayOutputStream.toByteArray();
            outputStream.write(bytes);
            outputStream.flush();
            byteArrayOutputStream.close();
            outputStream.close();
        }
    }


}
