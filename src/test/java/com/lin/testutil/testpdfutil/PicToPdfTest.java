package com.lin.testutil.testpdfutil;

import com.itextpdf.text.DocumentException;
import com.lin.util.FileWriteUtil;
import com.lin.util.pdfutil.PicToPdfUtil;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 图片转pdf
 *
 * @author linqiankun
 */
public class PicToPdfTest {

    private static final String filePath = "temp/";


    /**
     * test pass
     *
     * @throws IOException
     */
    @Test
    public void testOne() throws IOException {
        FileInputStream in = new FileInputStream(filePath + "0.jpg");
        String pdfPath = filePath + "0.pdf";
        FileWriteUtil.canCreatFile(pdfPath);
        FileOutputStream outputStream = new FileOutputStream(pdfPath);
        ByteArrayOutputStream byteArrayOutputStream = PicToPdfUtil.picToPdf1(in);
        byteArrayOutputStream.writeTo(outputStream);
        byteArrayOutputStream.close();
        outputStream.close();
        in.close();
    }


    /**
     * test pass
     *
     * @throws IOException
     */
    @Test
    public void testTwo() throws IOException {
        String imgPath = filePath + "1.jpg";
        String pdfPath = filePath + "1.pdf";
        PicToPdfUtil.imgToPdf2(imgPath, pdfPath);
    }


    /**
     * test pass
     *
     * @throws IOException
     * @throws DocumentException
     */
    @Test
    public void testThree() throws IOException, DocumentException {
        String imgPath = filePath + "2.jpg";
        String pdfPath = filePath + "2.pdf";
        PicToPdfUtil.imageToPdf3(imgPath, pdfPath);
    }


}
