package com.lin.testutil.testpdfutil;

import com.itextpdf.text.DocumentException;
import com.lin.util.FileWriteUtil;
import com.lin.util.pdfutil.MultiPdfToOnePdfUtil;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * 多合一
 *
 * @author linqiankun
 */
public class MultiToOneTest {

    private static final String filePath = "temp/";

    /**
     * test pass
     */
    @Test
    public void testOne() {
        String pdfPath = filePath + "mutil1.pdf";
        FileWriteUtil.canCreatFile(pdfPath);
        MultiPdfToOnePdfUtil.picToPdf(filePath, pdfPath);
    }


    /**
     * test pass
     *
     * @throws IOException
     * @throws DocumentException
     */
    @Test
    public void testTwo() throws IOException, DocumentException {
//        FileInputStream in = new FileInputStream("/Users/linqiankun/Documents/需求文件/法催3.4的副本.pdf");
//        FileInputStream ins = new FileInputStream("/Users/linqiankun/Documents/需求文件/法催3.4的副本2.pdf");
//        byte[] buf = new byte[in.available()];
//        byte[] bufs = new byte[ins.available()];
//        int read = in.read(buf);
//        int read1 = ins.read(bufs);

        File file = new File(filePath);
        boolean directory = file.isDirectory();
        List<byte[]> bytes = new LinkedList<>();
        if (directory) {
            File[] files = file.listFiles();
            for (File f : files) {
                if (f.isFile() && f.getName().toLowerCase().endsWith(".pdf")) {
                    FileInputStream inputStream = new FileInputStream(f);
                    byte[] buf = new byte[inputStream.available()];
                    int read = inputStream.read(buf);
                    bytes.add(buf);
                }
            }
        }

        byte[] bytes1 = MultiPdfToOnePdfUtil.pdfmMix(bytes);

        String pdf = filePath + "mutil2.pdf";

        FileWriteUtil.canCreatFile(pdf);
        FileOutputStream o = new FileOutputStream(pdf);
        o.write(bytes1);

        o.flush();
        o.close();

    }


}
