package com.lin.testutil;

import com.lin.util.XmlParseUtil;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Xml相关测试工具
 *
 * @author linqiankun
 */
public class XmlTest {

    @Test
    public void xmlParseTest() throws ParserConfigurationException, IOException, SAXException {
        String filepath = "src/test/resources/book.xml";
        XmlParseUtil.ReadXml(filepath, "book");

//        InputStream is = this.getClass().getResourceAsStream("/book.xml"); // 拿到资源
//        InputStream in = this.getClass().getClassLoader().getResourceAsStream("book.xml");
//        URL resource = this.getClass().getClassLoader().getResource("book.xml");
//        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//        DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
//        documentBuilder.parse(FileReadUtil.readFileToString(filepath));
//        documentBuilder.parse(String.valueOf(resource));
    }


}
