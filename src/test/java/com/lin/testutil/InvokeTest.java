package com.lin.testutil;

import com.lin.dto.BookDto;
import com.lin.dto.SenseCallbackDto;
import com.lin.dto.TestDto;
import com.lin.util.InvokeUtil;
import com.lin.util.XmlParseUtil;
import org.junit.Test;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 反射测试
 *
 * @author linqiankun
 */
public class InvokeTest {

    /**
     * test pass
     */
    @Test
    public void setFromMapTest() {
        String filepath = "src/test/resources/book.xml";
        List<BookDto> l = new ArrayList<>();
        NodeList book = XmlParseUtil.getNodeList(filepath, "book");
        for (int i = 0; i < book.getLength(); i++) {
            Node item = book.item(i);
            BookDto bookDto = new BookDto();
            //获取节点所有子节点值
            Map childNodeValueByNode = XmlParseUtil.getChildNodeValueByNode(item);
            InvokeUtil.setFromMap(bookDto, childNodeValueByNode);
            l.add(bookDto);
        }
        System.out.println(l.toString());
    }

    /**
     * test pass
     */
    @Test
    public void testDto(String[] args) throws IllegalAccessException {
        SenseCallbackDto dto = new SenseCallbackDto();
        dto.setBatchId("111");
        dto.setAudio("a");
        dto.setCallAnswerTime("s");
        dto.setCallEndTime("we");
        dto.setCallStartTime("iu");
        dto.setDialogue("ghj");
        dto.setEndFlow("gh");
        dto.setIvrResult("fgh");
        dto.setJobId("hsd");
        dto.setLoanUsername("nm");
        dto.setPhone("vbnm,");
        dto.setPlatformNo("cvbnm");
        dto.setResult("cvbnm");
        dto.setTemplateCode("ghjk,");
        dto.setResultCode(1);

        TestDto t = new TestDto();

        Class c = t.getClass();
        Field[] fields = c.getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);

            System.out.println(f.getType().getName());
            System.out.println(f.getGenericType().getTypeName());
            String substring = f.toString().substring(f.toString().lastIndexOf(".") + 1);
            System.out.println(f.toString());

        }
    }


}
