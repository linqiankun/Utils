package com.lin.testutil;

import com.alibaba.fastjson.JSON;
import com.lin.dto.BookDto;
import com.lin.util.ExcelExportUtil;
import com.lin.util.ExcelImportUtil;
import com.lin.util.FileWriteUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * excel测试
 *
 * @author linqiankun
 */
public class ExcelTest {

    private static final String filePath = "temp/";

    /**
     * test pass
     *
     * @throws IllegalAccessException
     * @throws IOException
     */
    @Test
    public void exportExcelTest() throws IllegalAccessException, IOException {
        String newFilePath = filePath + "testExcel.xls";
        BookDto b = new BookDto();
        b.setAuthor("cvbnm");
        b.setLanguage("english");
        b.setName("ihii");
        b.setPrice(90);
        b.setYear("2020");
        List<BookDto> l = new ArrayList<>();
        l.add(b);
        String[] str = {"name", "author", "price", "language", "year"};

        HSSFWorkbook excel = ExcelExportUtil.getExcel(l, str);

        File file = FileWriteUtil.canCreatFile(newFilePath);
        FileOutputStream out = new FileOutputStream(file);
        excel.write(out);

    }

    /**
     * test pass
     *
     * @throws FileNotFoundException
     */
    @Test
    public void importExcelTest() throws FileNotFoundException {
        String newFilePath = filePath + "testExcel.xls";
        File file = FileWriteUtil.canCreatFile(newFilePath);
        FileInputStream in = new FileInputStream(file);
        List<String[]> strings = ExcelImportUtil.readExcelFile(in, 1, 2);//从第二行开始读，就是2
        System.out.println(JSON.toJSONString(strings));

    }


}
