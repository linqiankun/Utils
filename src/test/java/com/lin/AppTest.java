package com.lin;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {

        char b = 'b';
        for (int i = 0; i < 10; i++) {
            switch (b) {
                case 'a':
                    break;
                case 'b':
                    continue;
                default:
                    break;
            }
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        String st1 = "hello";
        String ste = "he" + new String("llo");
        System.out.println(ste==st1);
    }

}
