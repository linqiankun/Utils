# Utils

## 介绍
想做点工具玩玩

## 软件架构
软件架构说明

1.  maven
2.  java SE

## 安装教程

1.  直接拷贝工具类代码嵌入自己项目即可
2.  [克隆地址](https://gitee.com/linqiankun/Utils.git)
3.  [文档介绍](https://gitee.com/linqiankun/Utils/wikis/Home)

## 使用说明

1. 直接拷贝工具类代码嵌入自己项目即可
2. 目前已完成一部分，经测试可以使用
   1. util
      1. Excel导出工具（ExcelExportUtil）
      2. EXcel导入工具（ExcelImportUtil）
      3. 文件读取工具（FileReadUtil）
      4. 文件写入工具（FileWriteUtil）
      5. Http(Https)连接工具（HttpConnectionUtil、HttpUrlConnectionUtil）
      6. Xml解析工具（XmlParseUtil）
      7. 反射工具（InvokeUtil）
      8. 省份城市工具（ProvinceCityUtil）
      9. 汉字转拼音工具（ChineseToEnglish2Util）
      10. 人名币转大写工具（RmbUppercaseUtil）
   2. pdfutil
      1. 多个pdf合成工具（MutilPdfToOnePdfUtil）
      2. pdf转图片工具（PdfToPicUtil）
      3. 图片转pdf工具，多图片转pdf工具（PicToPdfUtil）
3. 扩展工具，已完成可以直接使用
   1. 排序算法
      1. 冒泡排序（BubbleSort）
      2. 选择排序（SelectionSort）
      3. 插入排序（InsertionSort）
      4. 计数排序（CountingSort）
      5. 归并排序（MergeSort）
      6. 希尔排序（ShellSort）
      7. 堆排序（HeapSort）
      8. 基数排序（RadixSort）
      9. 桶排序（BucketSort）
      10. 快速排序（QuickSort）
   2. 树结构
      1. AVL树实现算法1（AVLTree1）
      2. 红黑树实现算法2（RBTree2）
   3. io模型
      1. NIO拷贝（Stream）
      2. BIO拷贝（Channel） 
      3. NIO读取（Test）（NioRead）
      4. NIO写入（Test）（NioWrite）
